package com.grosik.words.words.service;

import com.grosik.words.words.model.Word;

import java.util.List;

public interface IWordService {

    void addWords(Word word) throws Exception;

    List<Word> getWords(Long id);

    Word getWord(Long wordid) throws Exception;

    void update(Word word);

    Word randomWord(Long languageid);

    void addWord(Word word);

    List<Word> getAllWords();

}
