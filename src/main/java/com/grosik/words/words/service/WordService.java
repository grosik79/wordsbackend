package com.grosik.words.words.service;

import com.grosik.words.words.exceptions.WordsNotFoundException;
import com.grosik.words.words.model.Language;
import com.grosik.words.words.model.Word;
import com.grosik.words.words.repository.LanguageRepository;
import com.grosik.words.words.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class WordService implements IWordService {
    private WordRepository wordRepository;
    private IAppUserService appUserService;
    @Autowired
    private LanguageRepository langRepository;

    @Autowired
    public WordService(WordRepository wordRepository, IAppUserService appUserService) {
        this.wordRepository = wordRepository;
        this.appUserService = appUserService;
    }

    @Override
    public void addWords(Word word) throws Exception {
        Optional<Word> wordOptional = wordRepository.findByOriginal(word.getOriginal().toLowerCase());
        if (!wordOptional.isPresent()) {
            wordRepository.save(word);
        } else {
            throw new Exception("Word exists.");
        }
    }


    @Override
    public List<Word> getWords(Long id) {
        return wordRepository.findAllById(id);
    }

    @Override
    public Word getWord(Long wordid) throws Exception {
        Optional<Word> word = wordRepository.findById(wordid);
        if (!word.isPresent()) throw new Exception("Word does not exist.");
        return word.get();
    }

    @Override
    public void update(Word word) {
        wordRepository.save(word);
    }

    @Override
    public Word randomWord(Long languageid) {
        Optional<Language> lang = langRepository.findById(languageid);
        if (lang.isPresent()) {
            Language language = lang.get();
            List<Word> originalWords = new ArrayList<>();
            language.getTranslationSet().forEach(wordTranslation -> originalWords.add(wordTranslation.getWord()));

            return originalWords.get(new Random().nextInt(originalWords.size()));
        }
        throw new WordsNotFoundException();
    }

    @Override
    public void addWord(Word word) {
        wordRepository.save(word);
    }

    @Override
    public List<Word> getAllWords() {
        return wordRepository.findAll();//.stream().map(word -> word.getOriginal()).collect(Collectors.toList());
    }
}
