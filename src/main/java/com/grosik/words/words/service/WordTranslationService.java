package com.grosik.words.words.service;

import com.grosik.words.words.model.WordTranslation;
import com.grosik.words.words.repository.WordTranslationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WordTranslationService implements IWordTranslationService {

    @Autowired
    private WordTranslationRepository wordTranslationRepository;
    @Override
    public WordTranslation saveTranslation(WordTranslation translation) {
        return wordTranslationRepository.save(translation);
    }
}
