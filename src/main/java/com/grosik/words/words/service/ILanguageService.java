package com.grosik.words.words.service;

import com.grosik.words.words.model.Language;

import java.util.List;

public interface ILanguageService {
    List<Language> getAllLanguages();

    void saveLanguage(Language language);
}
