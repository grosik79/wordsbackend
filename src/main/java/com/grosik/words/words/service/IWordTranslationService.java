package com.grosik.words.words.service;

import com.grosik.words.words.model.WordTranslation;

public interface IWordTranslationService {
    WordTranslation saveTranslation(WordTranslation translation);
}
