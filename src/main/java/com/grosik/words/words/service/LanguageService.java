package com.grosik.words.words.service;


import com.grosik.words.words.model.Language;
import com.grosik.words.words.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LanguageService implements ILanguageService {

    private LanguageRepository languageRepository;
    private IAppUserService appUserService;

    @Autowired
    public LanguageService(LanguageRepository languageRepository, IAppUserService appUserService) {
        this.languageRepository = languageRepository;
        this.appUserService = appUserService;
    }

    public Language getLanguage(Long id) throws Exception {
        Optional<Language> languageOptional = languageRepository.findById(id);
        if (!languageOptional.isPresent()) throw new Exception("Language does not exist.");

        return languageOptional.get();
        // todo: obsługa exception - jesli languageOptional isPresent (nie jest obecny)
    }

    public void addLanguage(Language language) throws Exception {
        Optional<Language> languageOptional = languageRepository.findByName(language.getName().toLowerCase());
        if (!languageOptional.isPresent()) {
            languageRepository.save(language);
        } else {
            throw new Exception("Language is exist.");
        }
    }

    @Override
    public List<Language> getAllLanguages() {
        return languageRepository.findAll();//.stream().map(language -> language.getName()).collect(Collectors.toList());
    }

    @Override
    public void saveLanguage(Language language) {
        languageRepository.save(language);
    }
}



