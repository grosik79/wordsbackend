package com.grosik.words.words.service;

import com.grosik.words.words.exceptions.RegistrationException;
import com.grosik.words.words.model.AppUser;
import com.grosik.words.words.model.dto.PageResponse;

public interface IAppUserService {
    void register(AppUser appUser) throws RegistrationException;

    PageResponse<AppUser> getAllUsers();

    PageResponse<AppUser> getUsers(int page);
}
