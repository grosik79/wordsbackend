package com.grosik.words.words.controller;

import com.grosik.words.words.exceptions.RegistrationException;
import com.grosik.words.words.model.AppUser;
import com.grosik.words.words.model.dto.PageResponse;
import com.grosik.words.words.model.dto.RespFactory;
import com.grosik.words.words.model.dto.Response;
import com.grosik.words.words.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/")
@CrossOrigin
public class AppUserController {

    private AppUserService appUserService;

    @Autowired

    public AppUserController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<Response> register(@RequestBody AppUser appUser) {
        try {
            appUserService.register(appUser);
        } catch (RegistrationException e) {
            return RespFactory.badRequest();
        }

        return RespFactory.created();
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity<Response> list() {
        PageResponse<AppUser> list = appUserService.getAllUsers();

        return RespFactory.result(list);
    }
}
