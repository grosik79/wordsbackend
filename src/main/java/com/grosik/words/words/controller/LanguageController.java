package com.grosik.words.words.controller;

import com.grosik.words.words.model.Language;
import com.grosik.words.words.model.dto.RespFactory;
import com.grosik.words.words.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/lang/")
public class LanguageController {

    private LanguageService languageService;

    @Autowired
    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @RequestMapping(path = "/addLangu", method = RequestMethod.POST)
    public void addLangu(@RequestBody Language language) throws Exception {
        languageService.addLanguage(language);
    }

    @RequestMapping(path = "/list")
    public ResponseEntity<List<Language>> getLanguages(){
        List<Language> languages = languageService.getAllLanguages();
        return RespFactory.result(languages);
    }
}
