package com.grosik.words.words.controller;

import com.grosik.words.words.model.Word;
import com.grosik.words.words.model.dto.RespFactory;
import com.grosik.words.words.model.dto.WordDto;
import com.grosik.words.words.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/word/")
public class WordController {
    private WordService wordService;

    @Autowired

    public WordController(WordService wordService) {
        this.wordService = wordService;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public void addWords(@RequestBody WordDto word) throws Exception {
       wordService.addWords(new Word(word.getWord().toLowerCase()));
    }

    @RequestMapping(path = "/addword", method = RequestMethod.POST)
    public void addWord(@RequestBody Word word) throws Exception {
        wordService.addWord(word);
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public ResponseEntity<Word> getWords(@RequestParam(name = "id") Long wordid){
        try {
            return ResponseEntity.ok(wordService.getWord(wordid));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }
    @RequestMapping(path = "/list")
    public ResponseEntity<List<Word>> getWords(){
        List<Word> words = wordService.getAllWords();
        return RespFactory.result(words);
    }
    @RequestMapping(path ="/random")
    public ResponseEntity<Word> randomWord(@RequestParam(name="id") Long languageid){
        Word words = wordService.randomWord(languageid);
        return RespFactory.result(words);


    }

    // TODO: zapytanie do listy. Analogicznie jak w languagecontroller pobralismy liste jezykow, tak teraz
    // todo: powinienes zrobic zapytanie o wszystkie dostepne slowa, a nastepnie zmapować kazde slowo i wyciagnac z niego 'original'.
    // todo: po otrzymaniu original odeslij je jako lista string'ow w response entity. po stronie frontu zrob analogicznie jak
    // todo: z jezykami zapytanie fetchWords i dodaj kontrolkę (podobnie jak w languages) listę rozwijaną z ktorej wybieramy slowo ktorego tlumaczenie uzupelniamy.



//    @RequestMapping(path = "/delete", method = RequestMethod.DELETE)
//    public void delWords(@RequestBody Word word){
//        wordService.delWords(null);

//    }
}
