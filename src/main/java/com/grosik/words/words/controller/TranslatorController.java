package com.grosik.words.words.controller;

import com.grosik.words.words.exceptions.TranslatorException;
import com.grosik.words.words.model.Language;
import com.grosik.words.words.model.Word;
import com.grosik.words.words.model.WordTranslation;
import com.grosik.words.words.model.dto.TranslationDto;
import com.grosik.words.words.service.IWordTranslationService;
import com.grosik.words.words.service.LanguageService;
import com.grosik.words.words.service.WordService;
import com.grosik.words.words.service.WordTranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(path = "/trans/")
public class TranslatorController {

    private WordService wordService;
    private LanguageService languageService;
    @Autowired
    private IWordTranslationService wordTranslationService;

    @Autowired
    public TranslatorController(WordService wordService, LanguageService languageService) {
        this.wordService = wordService;
        this.languageService = languageService;
    }


    @RequestMapping(path = "/addTrans", method = RequestMethod.POST)
    public void addTrans(@RequestBody TranslationDto wordTranslationDTO) {
        try {
            Word word = wordService.getWord(wordTranslationDTO.getIdWord());
            Language language = languageService.getLanguage(wordTranslationDTO.getLanguageId());

            if (!word.getTranslationSet().stream().anyMatch(s -> s.getLanguage().equals(language))) {
                WordTranslation translation = new WordTranslation(wordTranslationDTO.getTranslation(), word, language);
                translation = wordTranslationService.saveTranslation(translation);
                word.getTranslationSet().add(translation);
                wordService.update(word);

                language.getTranslationSet().add(translation);
                languageService.saveLanguage(language);
            } else {
                throw new TranslatorException();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
//    @RequestMapping(path = "/del", method = RequestMethod.DELETE)
//    public void delTrans(@RequestBody WordTranslation wordTranslation){
//        wordTranslation.getWord(wordTranslation.delWord(null));
//    }

}
