package com.grosik.words.words.repository;

import com.grosik.words.words.model.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByLogin(String login);

    Optional<AppUser> findByEmail(String email);

    Page<AppUser>findAllBy(Pageable pageable);
}
