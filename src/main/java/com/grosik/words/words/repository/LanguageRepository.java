package com.grosik.words.words.repository;

import com.grosik.words.words.model.Language;
import com.grosik.words.words.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface
LanguageRepository extends JpaRepository<Language, Long> {
    List<Language>findAllBy();
    Optional<Language> findByName(String name);
}
