package com.grosik.words.words.repository;

import com.grosik.words.words.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WordRepository extends JpaRepository<Word, Long> {
    List<Word>findAllById(Long id);
    Optional<Word> findByOriginal(String original);

}