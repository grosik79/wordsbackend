package com.grosik.words.words.repository;

import com.grosik.words.words.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
