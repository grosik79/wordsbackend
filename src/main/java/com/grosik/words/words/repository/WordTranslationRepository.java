package com.grosik.words.words.repository;

import com.grosik.words.words.model.Word;
import com.grosik.words.words.model.WordTranslation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WordTranslationRepository extends JpaRepository<WordTranslation,Long> {
    List<WordTranslation>findAll();
}