package com.grosik.words.words.component;

import com.grosik.words.words.model.AppUser;
import com.grosik.words.words.model.Role;
import com.grosik.words.words.repository.AppUserRepository;
import com.grosik.words.words.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class DataInitializer {
    private RoleRepository roleRepository;
    private AppUserRepository appUserRepository;

    @Autowired
    public DataInitializer(RoleRepository roleRepository, AppUserRepository appUserRepository) {
        this.roleRepository = roleRepository;
        this.appUserRepository = appUserRepository;
        loadData();
    }

    private void loadData() {
        Role adminRole = new Role("ADMIN");
        roleRepository.save(adminRole);
        roleRepository.save(new Role("USER"));
        roleRepository.save(new Role("GUEST"));

        appUserRepository.save(new AppUser("admin", "admin", adminRole));
    }
}