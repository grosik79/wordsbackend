package com.grosik.words.words.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor

public class WordTranslation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String translation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Word word;

    @ManyToOne
    private Language language;

    public WordTranslation(String translation, Word word, Language language) {
        this.translation = translation;
        this.word = word;
        this.language = language;
    }
}
