package com.grosik.words.words.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TranslationDto {
    private String translation;
    private Long idWord;
    private Long languageId;

}
