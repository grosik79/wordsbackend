package com.grosik.words.words.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Word {
    @Id
    @Column(name = "word_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String original;

    @OneToMany(mappedBy = "word")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<WordTranslation> translationSet;

    public Word(String word) {
        this.original = word;
    }
}
