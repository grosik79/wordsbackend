package com.grosik.words.words.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class MessageReposnse extends Response {
    private String message;
}
