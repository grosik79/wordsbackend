package com.grosik.words.words.model;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String login;
    private String email;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private double score;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;


    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JsonIgnore
    private Set<Role> roleSet;

    public AppUser(String login, String email, String password, double score, Set<Role> roleSet) {
        this.login = login;
        this.email = email;
        this.password = password;
        this.score = score;
        this.roleSet = roleSet;
    }

    public AppUser(String admin, String admin1, Role adminRole) {
    }
}

